//
// Created by mpolatcan-gyte_cse on 06.11.2016.
//
#include <opencv2/opencv.hpp>
#include <pthread.h>
#include <cstdio>
#include "HoughTransform.h"

using namespace std;
using namespace cv;
using namespace MPHoughTransform;

const int THREAD_SIZE = 2000;
static pthread_mutex_t locker = PTHREAD_MUTEX_INITIALIZER;

void * generateVotingSpace(void *param);

namespace MPHoughTransform {
    void HoughTransform::setAccumulatorSize(Mat image) {
        if (image.cols > image.rows) {
            accumulatorSize = image.cols;
        } else {
            accumulatorSize = image.rows;
        }
    }

    void HoughTransform::allocateAccumulator() {
        accumulator = new int**[accumulatorSize];

        for (int i = 0; i < accumulatorSize; ++i) {
            accumulator[i] = new int*[accumulatorSize];
        }

        for (int i = 0; i < accumulatorSize; ++i) {
            for (int j = 0; j < accumulatorSize; ++j) {
                accumulator[i][j] = new int[radiusLimit];
            }
        }

        // clearing accumulator
        for (int i = 0; i < accumulatorSize; ++i) {
            for (int j = 0; j < accumulatorSize; ++j) {
                for (int k = 0; k < radiusLimit; ++k) {
                    accumulator[i][j][k] = 0;
                }
            }
        }
    }

    vector<CircleParam> HoughTransform::voting() {
        vector<CircleParam> circles;
        vector<Point> allEdgePoints;

        for (int i = 0; i < getImageHeight(); ++i) {
            for (int j = 0; j < getImageWidth(); ++j) {
                if (image.at<uchar>(i, j) == 255) {
                    allEdgePoints.push_back(Point(j, i));
                }
            }
        }

        if (allEdgePoints.size() == 0)
            return circles;

        int threadCount = 0;
        pthread_attr_t attrs;
        pthread_t *threads = new pthread_t[THREAD_SIZE];
        HoughParam *params = new HoughParam[THREAD_SIZE];

        pthread_attr_init(&attrs);
        pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_JOINABLE);

        for (int m = 0; m < THREAD_SIZE; ++m) {
            params[m].houghTransform = this;

            for (int k = m * 5; k < (m + 1) * 5; ++k) {
                if (k < allEdgePoints.size()) {
                    params[m].edges.push_back(allEdgePoints.at(k));
                } else {
                    break;
                }
            }

            if (params[m].edges.size() != 0) {
                ++threadCount;
                pthread_create(&threads[m], &attrs, generateVotingSpace, &params[m]);
            }
        }

        pthread_attr_destroy(&attrs);

        for (int i = 0; i < threadCount; ++i) {
            pthread_join(threads[i], NULL);
        }

        delete[](threads);
        delete[](params);

        Mat votingSpace;
        votingSpace.create(240,320,CV_8UC3);
        votingSpace = Scalar::all(0);

        int max_x = 0;
        int max_y = 0;
        int max_r = 0;
        int maxRadius = getMaxRadius();
        int minRadius = getMinRadius();
        bool maxFounded = false;

        for (int i = 0; i < accumulatorSize; ++i) {
            for (int j = 0; j < accumulatorSize; ++j) {
                for (int k = minRadius; k < maxRadius; ++k) {
                    if (accumulator[i][j][k] > 15 &&
                        accumulator[max_x][max_y][max_r] < accumulator[i][j][k]) {
                        max_x = i;
                        max_y = j;
                        max_r = k;
                        maxFounded = true;
                    }
                }
            }
        }

        if (maxFounded) {
            for (int i = 0; i < accumulatorSize; ++i) {
                for (int j = 0; j < accumulatorSize; ++j) {
                    for (int k = minRadius; k < maxRadius; ++k) {
                        if (accumulator[max_x][max_y][max_r] == accumulator[i][j][k]) {
                            CircleParam otherCircles;
                            otherCircles.centerCircle = Point(j, i) * 4;
                            otherCircles.radius = k * 4;
                            circles.push_back(otherCircles);
                        }
                    }
                }
            }

            for (int i = 0; i < accumulatorSize; ++i) {
                for (int j = 0; j < accumulatorSize; ++j) {
                    for (int k = minRadius; k < maxRadius; ++k) {
                        if (accumulator[max_x][max_y][max_r] - accumulator[i][j][k] <= 2) {
                            circle(votingSpace, Point(j,i) * 4, k * 4, Scalar(255,255,255), 2);
                        }
                    }
                }
            }

            char point[20];
            sprintf(point, "A: %d B: %d", max_y * 4, max_x * 4);
            circle(votingSpace,Point(max_y,max_x) * 4,1,Scalar(0,0,255),2);
            putText(votingSpace, point, Point(10, 230), FONT_HERSHEY_SIMPLEX,
                0.5, Scalar(0,255,0), 2);

        }

        imshow("Hough space", votingSpace);

        return circles;
    }
}

void * generateVotingSpace(void *param) {
    pthread_mutex_lock(&locker);

    HoughParam *params = (HoughParam *)param;
    int edgeSize = params->edges.size();
    int accumulatorSize = params->houghTransform->getAccumulatorSize();
    int maxRadius = params->houghTransform->getMaxRadius();
    int minRadius = params->houghTransform->getMinRadius();
    int degreeStep = params->houghTransform->getDegreeStep();

    for (int j = 0; j < edgeSize; ++j) {
        for (int k = minRadius; k < maxRadius; ++k) {
            for (int l = 0; l < 360; l += degreeStep) {
                int a = params->houghTransform->calculateA(params->edges.at(j).x, k, l);
                int b = params->houghTransform->calculateB(params->edges.at(j).y, k, l);
                if ((a >= 0 && a < accumulatorSize) &&
                    (b >= 0 && b < accumulatorSize)) {
                    params->houghTransform->incrementAccumulatorCell(a, b, k);
                }
            }
        }
    }

    pthread_mutex_unlock(&locker);
}