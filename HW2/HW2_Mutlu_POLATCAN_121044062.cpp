//
// Created by mpolatcan-gyte_cse on 29.10.2016.
//

#include <opencv2/opencv.hpp>
#include <cstdio>
#include <vector>
#include <ctime>

#include "HoughTransform.h"

using namespace std;
using namespace cv;
using namespace MPHoughTransform;

int main() {
// fps counter begin
    time_t start, end;
    int counter = 0;
    double sec;
    double fps;
    int option;

    // fps counter end
    Mat image, gray, gauss, edge;
    char key = 0;

    // open the default camera
    VideoCapture capture(0);

    // check for failure
    if (!capture.isOpened()) {
        printf("Failed to open a video device or video file!\n");
        return 1;
    }

    cout << "Execution mode: 1-Continuous, 2-Step by Step" << endl;
    cin >> option;

    // Set capture device properties
    capture.set(CV_CAP_PROP_FRAME_WIDTH, 320);
    capture.set(CV_CAP_PROP_FRAME_HEIGHT, 240);

    while (key != 'q') {
        // fps counter begin
        if (counter == 0){
            time(&start);
        }


        key = waitKey(25);

        if (option == 2) {
            printf("Proceed to next step press any key");
            waitKey(0);
        }

        capture >> image;

        gray.create(image.rows, image.cols, CV_8UC1);
        resize(image, gauss, Size(), 0.25, 0.25, INTER_LINEAR);
        edge.create(gray.size(), gray.type());
        GaussianBlur(gauss, gauss, Size(9,9),2,2);
        cvtColor(gauss, gray, COLOR_BGR2GRAY);
        Canny(gray, edge, 30.0, 90.0, 3, false);

        HoughTransform circleDetector(edge,30,100);
        vector<CircleParam> circles = circleDetector.voting();

        // fps counter begin
        time(&end);
        counter++;
        sec = difftime(end, start);
        fps = counter/sec;

        if (counter > 30)
            printf("FPS: %.2f\n", fps);

        // overflow protection
        if (counter == (INT_MAX - 1000))
            counter = 0;

        char strFps[30];

        sprintf(strFps, "FPS %.2f", fps);

        putText(image,
                strFps,
                Point(10, 20), FONT_HERSHEY_SIMPLEX,
                0.75, Scalar(rand() % 255, rand() % 255, rand() % 255), 2, 8);

        for (int i = 0; i < circles.size(); ++i) {
            circle(image,
                   Point(circles.at(i).centerCircle.x + 4,
                         circles.at(i).centerCircle.y + 4),
                   1,
                   Scalar(0, 0, 255),
                   3,
                   8);
            circle(image,
                   Point(circles.at(i).centerCircle.x + 4,
                         circles.at(i).centerCircle.y + 4),
                   circles.at(i).radius,
                   Scalar(0, 255, 0),
                   3,
                   8);
        }

        imshow("Circle Detection", image);
    }
}


