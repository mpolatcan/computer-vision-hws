//
// Created by mpolatcan-gyte_cse on 06.11.2016.
//

#ifndef HOUGHTRANSFORM_H
#define HOUGHTRANSFORM_H

#define PI 3.1415

namespace MPHoughTransform {
    typedef struct {
        cv::Point centerCircle;
        int radius;
    } CircleParam;

    class HoughTransform {
    public:
        HoughTransform(cv::Mat image) {
            radiusLimit = static_cast<int>(sqrt(pow(image.rows,2) + pow(image.cols,2)));

            this->image = image;

            // Default values for minimum radius, maximum radius and sensitivity
            minRadius = 20;
            maxRadius = 40;

            degreeStep = 12;

            setAccumulatorSize(image);

            allocateAccumulator();
        }

        HoughTransform(cv::Mat image, int minRadius, int maxRadius) {
            radiusLimit = static_cast<int>(sqrt(pow(image.rows,2) + pow(image.cols,2)));

            this->image = image;

            if (minRadius == 0 && maxRadius == 0) {
                this->minRadius = 15;
                this->maxRadius = 100;

                degreeStep = 18;
            } else {
                this->minRadius = static_cast<int>(ceil(minRadius / 4));
                this->maxRadius = static_cast<int>(ceil(maxRadius / 4));

                degreeStep = 12;
            }

            setAccumulatorSize(image);

            allocateAccumulator();
        }

        ~HoughTransform() {
            for (int i = 0; i < accumulatorSize; ++i) {
                for (int j = 0; j < accumulatorSize; ++j) {
                    delete[](accumulator[i][j]);
                }
                delete[](accumulator[i]);
            }

            delete[](accumulator);
        }

        void incrementAccumulatorCell(int a, int b, int r) { accumulator[b][a][r] += 1; }
        int calculateA(int x, int r, int theta) {  return x - r * cos(theta * (PI / 180.0)); }
        int calculateB(int y, int r, int theta) { return y - r * sin(theta * (PI / 180.0)); }
        int getMaxRadius() const { return maxRadius; }
        int getMinRadius() const { return minRadius; }
        int getAccumulatorSize() const { return accumulatorSize; }
        int getImageHeight() const { return image.rows; }
        int getImageWidth() const { return image.cols; }
        int getDegreeStep() const { return degreeStep; }
        std::vector<CircleParam> voting();
    private:
        // --------- FUNCTIONS --------
        void setAccumulatorSize(cv::Mat image);
        void allocateAccumulator();
        // ----------------------------

        cv::Mat image;
        int*** accumulator;
        int accumulatorSize;
        int degreeStep;
        int radiusLimit;
        int minRadius;
        int maxRadius;
    };

    typedef struct {
        HoughTransform *houghTransform;
        std::vector<cv::Point> edges;
    } HoughParam;
}
#endif //HOUGHTRANSFORM_H
