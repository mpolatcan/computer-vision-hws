//
// Created by mpolatcan-gyte_cse on 13.10.2016.
//
#include <opencv2/opencv.hpp>
#include <cstdio>
#include <ctime>

using namespace std;
using namespace cv;

void findBrightestSpot(Mat grayscale, Mat& img);
Mat convertGrayscale(Mat& img);

int main() {
    // fps counter begin
    time_t start, end;
    int counter = 0;
    double sec;
    double fps;
    // fps counter end

//    /* ------------ VIDEO TEST ------------ */
    Mat image;
    char key = 0;

    // open the default  camera
    VideoCapture capture(0);

    // check for failure
    if (!capture.isOpened()) {
        printf("Failed to open a video device or video file!\n");
        return 1;
    }

    // Set capture device properties
    capture.set(CV_CAP_PROP_FRAME_WIDTH, 640);
    capture.set(CV_CAP_PROP_FRAME_HEIGHT, 480);

    while (key != 'q') {
        // fps counter begin
        if (counter == 0){
            time(&start);
        }
        // fps counter end

        key = waitKey(25);
        // get a new frame from camera;
        capture >> image;
        findBrightestSpot(convertGrayscale(image), image);

        // fps counter begin
        time(&end);
        counter++;
        sec = difftime(end, start);
        fps = counter/sec;

        if (counter > 30)
            printf("FPS: %.2f\n", fps);

        // overflow protection
        if (counter == (INT_MAX - 1000))
            counter = 0;

        ostringstream strs;
        strs << fps;

        putText(image, "FPS: " + strs.str(), Point(10, 40), FONT_HERSHEY_SIMPLEX,
                1.0, Scalar(0, 0, 255), 2, 8);
        imshow("Brightness map", image);
    }
    /*-----------------------------------------------------------*/
}

void findBrightestSpot(Mat grayscale, Mat& img) {
    uchar maxPoint = 0;
    uchar secondMaxPoint = 0;
    uchar thirdMaxPoint = 0;
    vector<Point> firstBrightestSpot,
                  secondBrightestSpot,
                  thirdBrightestSpot;

    for (int i = 0; i < img.rows; ++i) {
        for (int j = 0; j < img.cols; ++j) {
            if (maxPoint < grayscale.at<uchar>(i,j)) {
                maxPoint = grayscale.at<uchar>(i,j);
            }

            if (grayscale.at<uchar>(i,j) != maxPoint &&
                maxPoint - grayscale.at<uchar>(i,j) >= 30 &&
                maxPoint - grayscale.at<uchar>(i,j) < 70 &&
                secondMaxPoint < grayscale.at<uchar>(i, j)) {
                secondMaxPoint = grayscale.at<uchar>(i, j);
            }

            if (grayscale.at<uchar>(i,j) != maxPoint &&
                grayscale.at<uchar>(i,j) != secondMaxPoint &&
                maxPoint - grayscale.at<uchar>(i,j) > 70 &&
                maxPoint - grayscale.at<uchar>(i,j) < 110 &&
                thirdMaxPoint < grayscale.at<uchar>(i, j)) {
                thirdMaxPoint = grayscale.at<uchar>(i, j);
            }
        }
    }


    for (int i = 0; i < img.rows; ++i) {
        for (int j = 0; j < img.cols; ++j) {
            if (maxPoint - grayscale.at<uchar>(i,j) < 10) {
                firstBrightestSpot.push_back(Point(j,i));
            }
            if (maxPoint - grayscale.at<uchar>(i,j) >= 10 &&
                maxPoint - grayscale.at<uchar>(i,j) < 70) {
                secondBrightestSpot.push_back(Point(j,i));
            }

            if (maxPoint - grayscale.at<uchar>(i,j) > 90 &&
                maxPoint - grayscale.at<uchar>(i,j) < 120) {
                thirdBrightestSpot.push_back(Point(j,i));
            }
        }
    }

    if (firstBrightestSpot.size() != 0) {
        Point maxPointLocs = firstBrightestSpot.at(firstBrightestSpot.size()/2);
        Point firstLoc = firstBrightestSpot.at(0);
        Point lastLoc = firstBrightestSpot.at(firstBrightestSpot.size()-1);

        if ((lastLoc.y - firstLoc.y) > (lastLoc.x - firstLoc.x)) {
            circle(img, Point(maxPointLocs.x, maxPointLocs.y), (lastLoc.y - firstLoc.y) / 5, Scalar(0,255,0), 2, 8);
        } else {
            circle(img, Point(maxPointLocs.x, maxPointLocs.y), (lastLoc.x - firstLoc.x) / 5, Scalar(0,255,0), 2, 8);
        }

        putText(img, "1", Point(maxPointLocs.x - 10, maxPointLocs.y - 20), FONT_HERSHEY_SIMPLEX,
                1.0, Scalar(0, 255, 0), 2, 8);
    }

    if (secondBrightestSpot.size() != 0) {
        Point secondMaxPointLocs = secondBrightestSpot.at(secondBrightestSpot.size()/2);
        Point firstLoc = secondBrightestSpot.at(0);
        Point lastLoc = secondBrightestSpot.at(secondBrightestSpot.size()-1);

        if ((lastLoc.y - firstLoc.y) > (lastLoc.x - firstLoc.x)) {
            circle(img, Point(secondMaxPointLocs.x, secondMaxPointLocs.y), (lastLoc.y - firstLoc.y) / 5,
                   Scalar(0, 0, 255), 2, 8);

        } else {
            circle(img, Point(secondMaxPointLocs.x, secondMaxPointLocs.y), (lastLoc.x - firstLoc.x) / 5,
                   Scalar(0, 0, 255), 2, 8);
        }

        putText(img, "2", Point(secondMaxPointLocs.x, secondMaxPointLocs.y), FONT_HERSHEY_SIMPLEX,
                1.0, Scalar(0, 0, 255), 2, 8);
    }

    if (thirdBrightestSpot.size() != 0) {
        Point thirdMaxPointLocs = thirdBrightestSpot.at(thirdBrightestSpot.size()/2);
        Point firstLoc = thirdBrightestSpot.at(0);
        Point lastLoc = thirdBrightestSpot.at(thirdBrightestSpot.size()-1);

        if ((lastLoc.y - firstLoc.y) > (lastLoc.x - firstLoc.x)) {
            circle(img, Point(thirdMaxPointLocs.x, thirdMaxPointLocs.y), (lastLoc.y - firstLoc.y) / 5, Scalar(255, 0, 0), 2, 8);
        } else {
            circle(img, Point(thirdMaxPointLocs.x, thirdMaxPointLocs.y), (lastLoc.x - firstLoc.x) / 5, Scalar(255, 0, 0), 2, 8);
        }
        putText(img, "3", Point(thirdMaxPointLocs.x - 10, thirdMaxPointLocs.y - 20), FONT_HERSHEY_SIMPLEX,
                1.0, Scalar(255, 0, 0), 2, 8);
    }
}

Mat convertGrayscale(Mat& img) {
    Mat grayscale;

    grayscale.create(img.rows, img.cols, CV_8UC1);

    for (int i = 0; i < img.rows; ++i) {
        for (int j = 0; j < img.cols; ++j) {
                grayscale.at<uchar>(i,j) = (img.at<Vec3b>(i,j)[0] * 0.07 + img.at<Vec3b>(i,j)[1] * 0.72 +
                                            img.at<Vec3b>(i,j)[2] * 0.21);

        }
    }

    return grayscale;
}
