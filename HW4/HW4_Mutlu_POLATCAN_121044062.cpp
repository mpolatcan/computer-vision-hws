//
// Created by mpolatcan-gyte_cse on 09.01.2017.
//

#include <opencv2/opencv.hpp>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <time.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <pthread.h>

#define CARS_POSITIVE_PATH "Cars_Positive\\"
#define CARS_NEGATIVE_PATH "Cars_Negative\\"
#define MINIVANS_POSITIVE_PATH "Minivans_Positive\\"
#define MINIVANS_NEGATIVE_PATH "Minivans_Negative\\"
#define TRUCKS_POSITIVE_PATH "Trucks_Positive\\"
#define TRUCKS_NEGATIVE_PATH "Trucks_Negative\\"
#define BUSES_POSITIVE_PATH "Buses_Positive\\"
#define BUSES_NEGATIVE_PATH "Buses_Negative\\"
#define	WINDOW_SIZE Size(32,32)
#define VIDEO_FILENAME "M6_Motorway_Traffic.mp4"
#define BUS_SVM_FILENAME "bus_detector.yml"
#define TRUCK_SVM_FILENAME "truck_detector.yml"
#define MINIVAN_SVM_FILENAME "minivan_detector.yml"
#define CAR_SVM_FILENAME "car_detector.yml"
#define PASS_LINE_BEGIN Point(10,325)
#define PASS_LINE_END Point(300,325)
#define SVM_NUM 4
#define CAR_HOG_DESCRIPTOR 0
#define MINIVAN_HOG_DESCRIPTOR 1
#define BUS_HOG_DESCRIPTOR 2
#define TRUCK_HOG_DESCRIPTOR 3
#define INC_CAR_NUM 5
#define INC_MINIVAN_NUM 6
#define INC_BUS_NUM 7
#define INC_TRUCK_NUM 8

using namespace cv;
using namespace cv::ml;
using namespace std;

vector<string> getPaths(string directoryName);
void* prepareSVMs(void *params);
void* detectVehicle(void *params);
void loadImages(const string& typePath, vector<Mat>& imageList);
void convertToML(const vector<Mat>& trainSamples, Mat& trainData);
void getSVMDetector(const Ptr<SVM>& svm, vector<float>& hogDetector);
void computeHog(const vector<Mat>& imageList, vector<Mat> & gradientList, const Size& size);
void trainSVM(const vector<Mat>& gradientList, const vector<int>& labels, const string& svmFilename);
void drawLocations(Mat& originalFrame, vector<Rect>& locations, const Scalar& color);
void startDetector(const Size& size);

typedef struct {
    string svmFilename;
    string positiveDataPath;
    string negativeDataPath;
} SVMParams;

typedef struct {
    HOGDescriptor hogDescriptor;
    Mat modifiedFrame;
    Mat originalFrame;
    Scalar color;
    int increasedNum;
} VehicleDetectorParams;

static pthread_mutex_t svmLocker = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t detectorLocker = PTHREAD_MUTEX_INITIALIZER;

int minivanNum = 0;
int carNum = 0;
int truckNum = 0;
int busNum = 0;

int main( int argc, const char** argv )
{
    string svmFilenames[] = {CAR_SVM_FILENAME,MINIVAN_SVM_FILENAME,TRUCK_SVM_FILENAME,BUS_SVM_FILENAME };
    string positiveDataPaths[] = {CARS_POSITIVE_PATH,MINIVANS_POSITIVE_PATH,TRUCKS_POSITIVE_PATH,BUSES_POSITIVE_PATH};
    string negativeDataPaths[] = {CARS_NEGATIVE_PATH,MINIVANS_NEGATIVE_PATH,TRUCKS_NEGATIVE_PATH,BUSES_NEGATIVE_PATH};

    pthread_attr_t attrs;
    pthread_t *trafficSVMs = new pthread_t[SVM_NUM];
    SVMParams *svmParams = new SVMParams[SVM_NUM];

    pthread_attr_init(&attrs);
    pthread_attr_setdetachstate(&attrs,PTHREAD_CREATE_JOINABLE);

    for (int i = 0; i < SVM_NUM; ++i) {
        svmParams[i].svmFilename = svmFilenames[i];
        svmParams[i].positiveDataPath = positiveDataPaths[i];
        svmParams[i].negativeDataPath = negativeDataPaths[i];
        pthread_create(&trafficSVMs[i],&attrs,prepareSVMs,&svmParams[i]);
    }

    pthread_attr_destroy(&attrs);

    for (int i = 0; i < SVM_NUM; ++i) {
        pthread_join(trafficSVMs[i], NULL);
    }

    delete[](trafficSVMs);
    delete[](svmParams);

    startDetector(WINDOW_SIZE);
}

void startDetector(const Size& size)
{
    time_t start, end;
    int counter = 0;
    double sec;
    double fps;
    char key = 27;
    Mat originalFrame,
        restrictedFrame;
    Ptr<SVM> carSvm,
             minivanSvm,
             busSvm,
             truckSvm;
    VideoCapture video;
    vector<float> carHogDetector,
                  minivanHogDetector,
                  busHogDetector,
                  truckHogDetector;
    pthread_attr_t attrs;
    pthread_t *hogDetectors = new pthread_t[SVM_NUM];
    HOGDescriptor *hogDescriptors = new HOGDescriptor[SVM_NUM];
    VehicleDetectorParams *vehicleDetectorParams = new VehicleDetectorParams[SVM_NUM];
    Scalar colors[] = {Scalar(0,255,0), Scalar(0,0,255), Scalar(255,0,0), Scalar(240,220,210)};
    int increasedNums[] = {INC_CAR_NUM, INC_MINIVAN_NUM, INC_BUS_NUM, INC_TRUCK_NUM};

    pthread_attr_init(&attrs);
    pthread_attr_setdetachstate(&attrs,PTHREAD_CREATE_JOINABLE);

    // ------ Set window size of hog descriptors ------
    hogDescriptors[CAR_HOG_DESCRIPTOR].winSize = size;
    hogDescriptors[MINIVAN_HOG_DESCRIPTOR].winSize = size;
    hogDescriptors[BUS_HOG_DESCRIPTOR].winSize = size;
    hogDescriptors[TRUCK_HOG_DESCRIPTOR].winSize = size;

    // --------- Load SVMs of 4 types vehicles --------
    carSvm = StatModel::load<SVM>(CAR_SVM_FILENAME);
    minivanSvm = StatModel::load<SVM>(MINIVAN_SVM_FILENAME);
    busSvm = StatModel::load<SVM>(BUS_SVM_FILENAME);
    truckSvm = StatModel::load<SVM>(TRUCK_SVM_FILENAME);

    // -------- Get svm detectors of 4 types vehicles ------
    getSVMDetector(carSvm, carHogDetector);
    getSVMDetector(minivanSvm,minivanHogDetector);
    getSVMDetector(busSvm,busHogDetector);
    getSVMDetector(truckSvm,truckHogDetector);

    // -------- Set svm detectors of 4 types vehicles to their hog descriptors --------
    hogDescriptors[CAR_HOG_DESCRIPTOR].setSVMDetector(carHogDetector);
    hogDescriptors[MINIVAN_HOG_DESCRIPTOR].setSVMDetector(minivanHogDetector);
    hogDescriptors[BUS_HOG_DESCRIPTOR].setSVMDetector(busHogDetector);
    hogDescriptors[TRUCK_HOG_DESCRIPTOR].setSVMDetector(truckHogDetector);

    video.open(VIDEO_FILENAME);

    if( !video.isOpened() )
    {
        cerr << "Unable to open the video " << VIDEO_FILENAME << "!" << endl;
        exit( -1 );
    }

    bool end_of_process = false;
    while(!end_of_process)
    {
        // fps counter begin
        if (counter == 0){
            time(&start);
        }

        video >> originalFrame;
        if(originalFrame.empty())
            break;

        resize(originalFrame,originalFrame,Size(640,480));
        restrictedFrame = originalFrame.clone();
        resize(restrictedFrame,restrictedFrame,Size(320,240));


        for (int i = 0; i < restrictedFrame.rows; ++i) {
            for (int j = 0; j < restrictedFrame.cols; ++j) {
                if (i < (restrictedFrame.rows / 2) + 39 || j > restrictedFrame.cols / 2) {
                    restrictedFrame.at<Vec3b>(i,j)[0] = 0;
                    restrictedFrame.at<Vec3b>(i,j)[1] = 0;
                    restrictedFrame.at<Vec3b>(i,j)[2] = 0;
                }
            }
        }

        for (int i = 0; i < SVM_NUM; ++i) {
            vehicleDetectorParams[i].modifiedFrame = restrictedFrame;
            vehicleDetectorParams[i].originalFrame = originalFrame;
            vehicleDetectorParams[i].hogDescriptor = hogDescriptors[i];
            vehicleDetectorParams[i].color = colors[i];
            vehicleDetectorParams[i].increasedNum = increasedNums[i];
            pthread_create(&hogDetectors[i],&attrs,detectVehicle,&vehicleDetectorParams[i]);
        }

        for (int i = 0; i < SVM_NUM; ++i) {
            pthread_join(hogDetectors[i],NULL);
        }

        // fps counter begin
        time(&end);
        counter++;
        sec = difftime(end, start);
        fps = counter/sec;

        // overflow protection
        if (counter == (INT_MAX - 1000))
            counter = 0;

        char strFps[30];

        sprintf(strFps, "FPS %.2f", fps);

        stringstream carSs, minivanSs, busSs, truckSs;
        carSs << carNum;
        minivanSs << minivanNum;
        busSs << busNum;
        truckSs << truckNum;

        putText(originalFrame, "Car: " + carSs.str(), Point(150,20), FONT_HERSHEY_SIMPLEX, 0.75, colors[0],2);
        putText(originalFrame, "Minivan: " + minivanSs.str(), Point(250,20),FONT_HERSHEY_SIMPLEX,0.75,colors[1],2);
        putText(originalFrame, "Bus: " + busSs.str(), Point(400,20),FONT_HERSHEY_SIMPLEX,0.75,colors[2],2);
        putText(originalFrame, "Truck: " + minivanSs.str(), Point(500,20),FONT_HERSHEY_SIMPLEX,0.75,colors[3],2);
        putText(originalFrame, strFps, Point(10, 20), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(255, 0, 0), 2, 8);
        imshow("Traffic Detector", originalFrame);

        key = (char)waitKey(30);

        if(27 == key)
            end_of_process = true;
    }

    pthread_attr_destroy(&attrs);
    delete[](hogDetectors);
    delete[](vehicleDetectorParams);
    delete[](hogDescriptors);
}

void *prepareSVMs(void *param) {
    pthread_mutex_lock(&svmLocker);

    SVMParams *params = (SVMParams*)param;

    vector<Mat> positiveList;
    vector<Mat> fullNegativeList;
    vector<Mat> negativeList;
    vector<Mat> gradientList;
    vector<int> labels;

    loadImages(params->positiveDataPath, positiveList);
    labels.assign(positiveList.size(), +1);
    loadImages(params->negativeDataPath, fullNegativeList);
    labels.insert(labels.end(), fullNegativeList.size(), -1);
    computeHog(positiveList, gradientList, WINDOW_SIZE);
    computeHog(fullNegativeList, gradientList, WINDOW_SIZE);
    trainSVM(gradientList,labels,params->svmFilename);

    pthread_mutex_unlock(&svmLocker);
}

void* detectVehicle(void *params) {
    pthread_mutex_lock(&detectorLocker);
    VehicleDetectorParams *detectorParams = (VehicleDetectorParams*)params;

    vector<Rect> locations;
    detectorParams->hogDescriptor.detectMultiScale(detectorParams->modifiedFrame,locations,0,Size(),Size(),1.10);
    drawLocations(detectorParams->originalFrame,locations,detectorParams->color);

    for (int i = 0; i < locations.size(); ++i) {
        if ((locations.at(i).y + locations.at(i).height / 2) < PASS_LINE_BEGIN.y) {
            line(detectorParams->originalFrame,PASS_LINE_BEGIN,PASS_LINE_END,Scalar(0,255,0),3);

            if (detectorParams->increasedNum == INC_CAR_NUM) {
                ++carNum;
            } else if (detectorParams->increasedNum == INC_MINIVAN_NUM) {
                ++minivanNum;
            } else if (detectorParams->increasedNum == INC_BUS_NUM) {
                ++busNum;
            } else if (detectorParams->increasedNum == INC_TRUCK_NUM) {
                ++truckNum;
            }
        } else {
            line(detectorParams->originalFrame,PASS_LINE_BEGIN,PASS_LINE_END,Scalar(0,0,255),3);
        }
    }

    pthread_mutex_unlock(&detectorLocker);
}

vector<string> getPaths(string directoryName) {
    vector<string> paths;

    string filepath;
    DIR *dp;
    struct dirent *dirp;
    struct stat filestat;

    dp = opendir(directoryName.c_str());

    if (dp == NULL)
    {
        cout << "Error(" << errno << ") opening " << directoryName << endl;
        exit(0);
    }

    while ((dirp = readdir(dp)))
    {
        filepath = directoryName + "/" + dirp->d_name;

        if (stat(filepath.c_str(), &filestat)) {
            continue;
        }

        if (S_ISDIR( filestat.st_mode )) {
            continue;
        }

        paths.push_back(filepath);
    }

    closedir( dp );

    return paths;
}

void getSVMDetector(const Ptr<SVM>& svm, vector<float>& hogDetector )
{
    // get the support vectors
    Mat sv = svm->getSupportVectors();
    const int sv_total = sv.rows;
    // get the decision function
    Mat alpha, svidx;

    double rho = svm->getDecisionFunction(0, alpha, svidx);

    CV_Assert( alpha.total() == 1 && svidx.total() == 1 && sv_total == 1 );
    CV_Assert( (alpha.type() == CV_64F && alpha.at<double>(0) == 1.) ||
               (alpha.type() == CV_32F && alpha.at<float>(0) == 1.f) );
    CV_Assert( sv.type() == CV_32F );
    hogDetector.clear();

    hogDetector.resize(sv.cols + 1);
    memcpy(&hogDetector[0], sv.ptr(), sv.cols*sizeof(hogDetector[0]));
    hogDetector[sv.cols] = (float)-rho;
}


/*
* Convert training/testing set to be used by OpenCV Machine Learning algorithms.
* TrainData is a matrix of size (#samples x max(#cols,#rows) per samples), in 32FC1.
* Transposition of samples are made if needed.
*/
void convertToML(const vector<Mat>& trainSamples, Mat& trainData )
{
    //--Convert data
    const int rows = (int)trainSamples.size();
    const int cols = (int)std::max( trainSamples[0].cols, trainSamples[0].rows );
    cv::Mat tmp(1, cols, CV_32FC1); //< used for transposition if needed
    trainData = cv::Mat(rows, cols, CV_32FC1 );
    vector< Mat >::const_iterator itr = trainSamples.begin();
    vector< Mat >::const_iterator end = trainSamples.end();
    for( int i = 0 ; itr != end ; ++itr, ++i )
    {
        CV_Assert( itr->cols == 1 ||
                   itr->rows == 1 );
        if( itr->cols == 1 )
        {
            transpose( *(itr), tmp );
            tmp.copyTo( trainData.row( i ) );
        }
        else if( itr->rows == 1 )
        {
            itr->copyTo( trainData.row( i ) );
        }
    }
}

void loadImages(const string& typePath, vector<Mat>& imageList)
{
    vector<string> paths = getPaths(typePath);

    for (int i = 0; i < paths.size(); ++i) {
        Mat originalFrame = imread(paths.at(i));
        resize(originalFrame,originalFrame,WINDOW_SIZE);
        imageList.push_back(originalFrame.clone());
    }
}

void computeHog(const vector<Mat> & imageList, vector<Mat>& gradientList, const Size& size )
{
    HOGDescriptor hog;
    hog.winSize = size;
    Mat gray;
    vector< Point > location;
    vector< float > descriptors;

    vector< Mat >::const_iterator originalFrame = imageList.begin();
    vector< Mat >::const_iterator end = imageList.end();
    for( ; originalFrame != end ; ++originalFrame )
    {
        cvtColor( *originalFrame, gray, COLOR_BGR2GRAY );
        hog.compute( gray, descriptors, Size( 8, 8 ), Size( 0, 0 ), location );
        gradientList.push_back( Mat( descriptors ).clone() );
    }
}

void trainSVM(const vector<Mat>& gradientList, const vector<int>& labels, const string& svmFilename)
{
    Ptr<SVM> svm = SVM::create();
    /* Default values to train SVM */
    svm->setCoef0(0.0);
    svm->setDegree(3);
    svm->setTermCriteria(TermCriteria(TermCriteria::MAX_ITER, 100, 1e-3));
    svm->setGamma(0);
    svm->setKernel(SVM::LINEAR);
    svm->setNu(0.5);
    svm->setP(0.1); // for EPSILON_SVR, epsilon in loss function?
    svm->setC(0.01); // From paper, soft classifier
        svm->setType(SVM::EPS_SVR); // C_SVC; // EPSILON_SVR; // may be also NU_SVR; // do regression task

    Mat train_data;
    convertToML(gradientList, train_data);

    clog << "Start training...";
    svm->train(train_data, ROW_SAMPLE, Mat(labels));
    clog << "...[done]" << endl;

    svm->save(svmFilename);
}

void drawLocations(Mat& originalFrame, vector<Rect>& locations, const Scalar& color)
{
    if (!locations.empty())
    {
        for (int i = 0; i < locations.size(); ++i)
        {
            locations.at(i).x = locations.at(i).x * 2 + 5;
            locations.at(i).y = locations.at(i).y * 2 + 5;
            locations.at(i).width = (int)ceil(locations.at(i).width * 1.5f);
            locations.at(i).height = (int)ceil(locations.at(i).height * 1.5f);
            rectangle(originalFrame, locations.at(i), color, 2);
        }
    }
}

